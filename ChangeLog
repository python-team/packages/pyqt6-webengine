2024-10-24  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS:
	Updated the NEWS file.
	[33da3a9ed997] [6.8.0]

2024-10-14  Phil Thompson  <phil@riverbankcomputing.com>

	* pyproject.toml:
	Updated the build system requirements to SIP v6.9 and PyQt-builder
	v1.17.
	[2ca014817aec]

2024-10-04  Phil Thompson  <phil@riverbankcomputing.com>

	* PyQt6-WebEngine.msp, mksccode/QtWebEngineCore.versions:
	Updated for Qt v6.8.0.
	[a921a4c073f6]

2024-04-20  Phil Thompson  <phil@riverbankcomputing.com>

	* .hgtags:
	Added tag 6.7.0 for changeset cb8b0faaaac7
	[fc6338be5751]

2024-04-04  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Updated for Qt v6.7.0.
	[cb8b0faaaac7] [6.7.0]

2024-03-17  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp, mksccode/QtWebEngineCore.versions:
	Added support for Qt v6.7.
	[10ef78663bab]

2024-02-18  Phil Thompson  <phil@riverbankcomputing.com>

	* README, README.md, pyproject.toml:
	Migrated from [tool.sip.metadata] to [project] in pyproject.toml.
	[44c8b10a047b]

	* NEWS:
	Updated the NEWS file.
	[dfeb66ea536b]

	* Merged the 6.6-maint branch.
	[4ff980ee9851]

2024-01-02  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, rb-product.toml:
	Removed the product file.
	[b7be550b308f] <6.6-maint>

2023-10-25  Phil Thompson  <phil@riverbankcomputing.com>

	* .hgtags:
	Added tag 6.6.0 for changeset 6b4eda057dbf
	[35b28a4bc972]

	* NEWS, rb-product.toml:
	Removed an out of date entry in the product file.
	[6b4eda057dbf] [6.6.0]

2023-10-22  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Updated for Qt v6.6.
	[618f2f9800e9]

2023-04-04  Phil Thompson  <phil@riverbankcomputing.com>

	* .hgtags:
	Added tag 6.5.0 for changeset 105702805a7a
	[d3ffa12ad593]

2023-03-31  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Updated for Qt v6.5.0rc.
	[105702805a7a] [6.5.0]

2022-09-30  Phil Thompson  <phil@riverbankcomputing.com>

	* .hgtags:
	Added tag 6.4.0 for changeset 1514ce956b1e
	[0eff8a16b8ef]

	* NEWS:
	Updated for Qt v6.4.0.
	[1514ce956b1e] [6.4.0]

2022-09-21  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Updated for Qt v6.4.0rc.
	[dfc5e1aaac42]

2022-09-18  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS:
	Updated the NEWS file.
	[939405302b63]

	* Merged the 6.3-maint branch.
	[5e0f39f3325a]

2022-09-08  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Switched the order or the QWebEnginePage ctors so that a profile is
	not interpreted as the parent object.
	[ba280f841c39] <6.3-maint>

2022-06-17  Phil Thompson  <phil@riverbankcomputing.com>

	* .hgtags:
	Added tag 6.3.1 for changeset 4ff79187d623
	[83840fda4879] <6.3-maint>

2022-04-26  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Added qWebEngineVersion(), qWebEngineChromiumVersion(), and
	qWebEngineChromiumSecurityPatchVersion().
	[4ff79187d623] [6.3.1] <6.3-maint>

2022-04-12  Phil Thompson  <phil@riverbankcomputing.com>

	* .hgtags:
	Added tag 6.3.0 for changeset 233ae2b96b8a
	[396d9fbec4c3]

2022-03-31  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Updated for Qt v6.3.0rc.
	[233ae2b96b8a] [6.3.0]

2022-03-28  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS:
	Merged the 6.2-maint branch.
	[d2b0c7ca12b1]

2022-03-15  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Allow Qt.AA_ShareOpenGLContexts to be specified before a
	QCoreApplication is created to allow QtWebEngineWidgets to be
	imported later.
	[060f78886515] <6.2-maint>

2021-10-28  Phil Thompson  <phil@riverbankcomputing.com>

	* .hgtags:
	Added tag 6.2.1 for changeset b7db172105e0
	[28b353871f07] <6.2-maint>

	* NEWS, PyQt6-WebEngine.msp:
	Updated to the latest project format.
	[b7db172105e0] [6.2.1] <6.2-maint>

2021-10-03  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Added a missing '#include'.
	[fd4ef8c42b00] <6.2-maint>

2021-09-30  Phil Thompson  <phil@riverbankcomputing.com>

	* .hgtags:
	Added tag 6.2.0 for changeset 3bea54ff6aa7
	[207cbcd62885]

	* NEWS, PyQt6-WebEngine.msp:
	Updated for Qt v6.2.0.
	[3bea54ff6aa7] [6.2.0]

2021-09-17  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Updated for Qt v6.2.0rc.
	[39fe7f7f823b]

2021-08-26  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Fixed the QWebEnginePage.FindFlag, QWebEngineScript.ScriptWorldId
	and QWebEngineUrlScheme.Flag enums.
	[f6f76fc73a1d]

2021-08-25  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Added the sub-class convertor code.
	[51a5f1600320]

	* PyQt6-WebEngine.msp:
	All modules can now be imported.
	[98cb6c2cd8cc]

	* PyQt6-WebEngine.msp:
	All modules now compile.
	[04da79605ac9]

2021-08-24  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, PyQt6-WebEngine.msp:
	Completed the initial port of Qt v6.2.0beta3.
	[bcf2a75a1633]

2021-08-22  Phil Thompson  <phil@riverbankcomputing.com>

	* PyQt6-WebEngine.msp:
	Ported the majority of QtWebEngineCore.
	[62740a61322c]

	* PyQt6-WebEngine.msp, pyproject.toml:
	Completed QtWebEngineQuick and QtWebEngineWidgets.
	[622c5b61208b]

	* NEWS, PyQt6-WebEngine.msp, pyproject.toml:
	Initial restructuring of the modules for Qt v6.2.0.
	[994c8eca9f14]

2021-08-05  Phil Thompson  <phil@riverbankcomputing.com>

	* NEWS, mksccode/QtWebEngine.versions,
	mksccode/QtWebEngineCore.versions,
	mksccode/QtWebEngineWidgets.versions:
	Removed the legacy sub-class version files.
	[db7b01ff15c2]

2021-07-15  Phil Thompson  <phil@riverbankcomputing.com>

	* PyQt6-WebEngine.msp:
	Stripped legacy versions from the project file.
	[a274c1e046b0]

	* .hgignore, NEWS, PyQt6-WebEngine.msp, README,
	mksccode/QtWebEngine.versions, mksccode/QtWebEngineCore.versions,
	mksccode/QtWebEngineWidgets.versions, pyproject.toml, rb-
	product.toml:
	Initial commit based on PyQtWebEngine for PyQt5.
	[31e565a7caa4]
